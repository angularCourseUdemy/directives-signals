import { Component, inject, OnInit, signal } from '@angular/core';
import { User } from '../../interfaces/user.interface';
import { UsersServiceService } from '../../services/users-service.service';

@Component({
  templateUrl: './user-info-page.component.html',
  styleUrls: ['./user-info-page.component.css']
})
export class UserInfoPageComponent implements OnInit{

  ngOnInit(): void {
    this.loadUser(this.userId())
  }

  private usersService = inject(UsersServiceService)
  public userId = signal(1);
  public currentUser = signal<User | undefined>(undefined);
  public userWasFound = signal(true);

  loadUser(id: number){
    if(id<=0) return;
    this.userId.set(id)
    this.usersService.getUserById(id)
    .subscribe(user => (
      this.currentUser.set(user)
    )
    )
  }
}
